# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models


class ResCityZipLineWizard(models.TransientModel):

    _name = 'res.city.zip.line.wizard'
    _description = 'Manage Cedex'

    wizard_id = fields.Many2one('res.city.zip.line.wizard', 'Wizard')


class ResCityZipWizard(models.TransientModel):

    _name = 'res.city.zip.wizard'
    _description = 'Manage Cedex Wizard'

    @api.model
    def default_get(self, fields):
        res = super(ResCityZipWizard, self).default_get(fields)
        active_ids = self.env.context.get('active_ids')
        if (self.env.context.get('active_model') == 'res.city.zip'
                and active_ids):
            res['res_city_zip_ids'] = active_ids
        return res

    current_line_id = fields.Many2one(
        'res.city.zip.line.wizard', string='Current Line')
    line_ids = fields.One2many(
        'res.city.zip.line.wizard', 'wizard_id', string='Lines')
    res_city_zip_ids = fields.Many2many('res.city.zip', string='Zips')

    @api.multi
    def action_apply(self):
        if not self.res_city_zip_ids:
            return {
                'type': 'ir.actions.act_window',
                'res_model': self._name,
                'res_id': self.id,
                'view_mode': 'form',
                'target': 'new',
            }
        for zip_cedex in self.res_city_zip_ids:
            if ' CEDEX' in zip_cedex.name:
                zipori = zip_cedex.name
                position = zip_cedex.name.rfind(' CEDEX')
                zip_cedex.name = zip_cedex.name[0:position]
                zip_cedex.cedex = zipori[position+1:]
        return {'type': 'ir.actions.act_window_close'}
