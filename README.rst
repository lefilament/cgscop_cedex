.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


================================
CG SCOP - Gestion CEDEX
================================

Modifie le module Base Location:
    - ajout d'un champ CEDEX au modele res.city.zip 
    - ajout d'un bouton pour remplir automatiquement le champ CEDEX à partir du ZIP code
    - modifie la fonction onchange_zip pour remplir le champ CEDEX automatiquement  

Credits
=======

Funders
------------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Juliana Poudou <juliana@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
