{
    "name": "CG SCOP - Gestion CEDEX",
    "summary": "CG SCOP - Gestion CEDEX",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "base_location",
    ],
    "data": [
        "views/res_city_zip.xml",
        "wizard/res_city_zip_wizard.xml",
    ]
}
